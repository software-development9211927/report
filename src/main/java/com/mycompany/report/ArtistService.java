/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.report;

import java.util.List;

/**
 *
 * @author Pond
 */
public class ArtistService {
    public List<ArtistReport> getTopTenArtistByTotalPrice() {
        artistDao artistDao = new artistDao();
        return artistDao.getArtistByTotalPrice(10);
    }
    
    public List<ArtistReport> getTopTenArtistByTotalPrice(String begin,String end) {
        artistDao artistDao = new artistDao();
        return artistDao.getArtistByTotalPrice(begin,end,10);
    }
}
